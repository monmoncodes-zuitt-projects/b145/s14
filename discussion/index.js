// this is a single line comment
console.log("Hello from JS");

/*  multi-line comment
console.log("Hello from JS");
console.log("Hello from JS");
console.log("Hello from JS");
console.log("Hello from JS");
*/
console.log("example statment");

// syntax: console.log(value/message);

// [DECLARING VARIABLES]
// - tell our devices that a variable name is created and ready and ready to store data .

// syntax: let/const desiredVariableName;

let myVariable;

// WHAT is a variable
// it is used to contain/store data.

// Benefits of utilizing a variable
// This will make it easier for us to associate information stored in our devices to actual names about the information.
// an assignment operator (=) is used to assign/pass down values into a variable
let clientName = "Juan Dela Cruz";
let contactNumber = "09999999999";

// [PEEKING INSIDE A VARIABLE]
let greetings = "Hello Batch 145";

console.log(clientName);
console.log(contactNumber);
console.log(greetings);
// [DATA TYPES]
// 1. String - contain one or more alphanumeric chracters
let country = "Philippines";
let province = 'Metro Manila';
// 2. Numbers - include positive, negative decimals
let headcount = 30;
let grade = 98.5;
//3. Boolean- contains two logical values that answers the question yes or no/ true or false
let isMarried = false;
let inGoodConduct = true;
//4.Null - indicates the absence of a value.
let spouse = null;
console.log(spouse);
// 5. Undefined- represents the state of a variable that has been declared but without an assign value.
let fullName;
// Arrays are a special kind of data type that's used to store multiple values. Arrays can store different data types but is normally used to store similar data types.
// Object contains other data(like a person's parts of a full name) or a set of information(array of personal information for different persons)
// string number and boolean data types are considered as primitive data types because it can only contain a single value.
// the object data is a composite data type since it can either hold multiple data regarding a real-world/imaginary object or a set of data(or commonly called as arrays)
// null and undefined data types are considered special data types due to what it represents.
// 3 Forms of data types:
// 1.Primitive
// 2.Composite
// 3.Special

// if we would try to print out a value of a variable that has not been declared a value, it will return an error of "undefined".
// six types of data: String, Numbers, Boolean, Null, Undefined, Objects

// Arrays
	// Arrays are a special kind of composite data type that is used to store multiple values.
   // []-describe an array container

   // lets create a collection of all your subjects in the bootcamp
  let bootcampSubjects = ["HTML","CSS","Bootstrap","Javascript"];

  // display the output of the array inside the console
console.log(bootcampSubjects);

// Rule of Thumb when declaring array structures
// Storing multiple data types inside an array is NOT recommended. in a context of programming, this doesn't make any sense.
// an array should be a collection of data that describes a similar/single topic or subject.

let details = ["Keannu","Reeves",32,true];
console.log(details);

// Objects- are another special kind of composite data type that is used to mimic or represent a real world object/item. They are used to create complex data that contains pieces of information that are relevant to each other. Every individual piece of code/information is called property of an object.

// SYNTAX:
// let/const objectName = {}
// key -> value
	// propertyA: value,
	// propertyB: value

// lets create an object that describes the properties of a cellphone

let cellphone = {
	brand:'Samsung',
	model:'S10',
	color:'Black',
	serialNo:'AX120021012',
	isHomeCredit: true,
	features:["Calling","Texting","Ringing","5G"],
	price: 8000
	
}

// lets display the object inside the console.
console.log(cellphone)

// Variables and Constants

// Variables - are used to store data. the values/info stored inside a variable can be changed or repackaged.
let personName = "Michael";
console.log(personName);
// If you're going to reassign a new value for a variable you no longer have to use the "let" again.
personName = "Jordan";
console.log(personName);

// concatenating string (+)
// join,combine,link
let pet = "dog";
console.log("this is the initial value of the var: " + pet);

pet = "cat";
console.log("This is the new value of the var: " + pet)

// Constants - the value assigned on a constant cannot be changed.
// Permanent, fixed, absolute
// syntax: const desiredName = "value";
const PI = 3.14;
console.log(PI);
const year = 12;
console.log(year);
const familyName = "Dela Cruz";
console.log(familyName);

// lets try to reassign a new value in a constant
// familyName = 'Gaw';
// console.log(Gaw);
